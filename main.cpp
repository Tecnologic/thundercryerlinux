/*
 * main.cpp
 *
 *  Created on: 11.01.2014
 *      Author: A
 */


#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>

int main (void){
  FILE* pipeMpc;
  pid_t pidMpc;
  int status;
  char buffer[256];

  printf("Execute mpc pause!\n\r");
  // open pipe to read the output from mpc
  pipeMpc = popen("mpc pause", "r");
  if(pipeMpc == NULL){
    printf("Error: opening mpc pipe!\n\r");
    return -1;
  }
  //wait for mpc pause to complete
  pidMpc = wait(&status);
  if(-1 <= pidMpc){
    printf("Error: wait returned error!\n\r");
    return pidMpc;
  }

  //read the output of mpc pause
  fread_unlocked(buffer, 1, 256, pipeMpc);

  // close the pipe
  pclose(pipeMpc);

  // print the pipe
  printf(buffer);

  return 0;

}


